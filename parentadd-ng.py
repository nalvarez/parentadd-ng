#!/usr/bin/python
# vim: set fileencoding=utf-8:

# Git parent-adder
# 
# Copyright © 2012 Nicolás Alvarez <nicolas.alvarez@gmail.com>
# 
# This program is free software, you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Refer to LICENSE.txt or http://www.gnu.org/licenses/ for the full
# license text.

from dulwich.repo import Repo
import dulwich.objects
import re
import sys

from tqdm import tqdm

class SvnParentMap(object):
    def __init__(self, f):
        self.data = {}
        self.load(f)

    def load(self, f):
        for line in f:
            if line[-1] == '\n':
                line = line[:-1]
            if line == '' or line.startswith("#"):
                continue

            words = line.split(" ")
            if len(words) != 2:
                raise RuntimeError("parentmap parse error")
            if words[0] == 'clear':
                raise NotImplementedError("'clear' keyword isn't implemented yet")
            else:
                rev1 = self.parseMapRev(words[0])
            rev2 = self.parseMapRev(words[1])
            if rev2 not in self.data:
                self.data[rev2] = []
            self.data[rev2].append(rev1)

    def revsUsed(self):
        """Returns a set of SVN revision numbers mentioned in this parentmap."""
        result = set()
        for key, parents in self.data.iteritems():
            result.add(key)
            result.update(parents)
        return result

    @staticmethod
    def parseMapRev(token):
        return int(token)

class ParentMapConverter(object):
    def __init__(self, parentmap):
        if not isinstance(parentmap, SvnParentMap):
            raise TypeError()
        self.svnpm = parentmap
        self.interestingRevs = parentmap.revsUsed()
        self.revMap = {}

    def feedCommit(self, commit):
        svnrev = getSvnRev(commit)
        if svnrev in self.interestingRevs:
            # interestingRevs is a set, so this should never ever happen
            assert svnrev not in self.revMap

            self.revMap[svnrev] = commit.id

    def doConversion(self):
        newmap = {}
        for key, parents in self.svnpm.data.iteritems():
            parentlist = []
            for parent in parents:
                parentlist.append(self.revMap[parent])
            newmap[self.revMap[key]] = parentlist
        return newmap

def getSvnRev(commit):
    '''
    Returns the SVN revision corresponding to the given commit.
    'commit' must be a dulwich.objects.Commit.
    '''
    # TODO: look at the last line only
    match = re.search(r"^svn path=([^; ]+); revision=(\d+)", commit.message, re.MULTILINE)
    if match:
        svnrev = int(match.group(2))
        return svnrev
    else:
        return None

if len(sys.argv) != 3:
    sys.stderr.write("usage: %s <repo> <parentmap>\n" % sys.argv[0])
    sys.exit(1)

repo = Repo(sys.argv[1])

svnParentmap = SvnParentMap(open(sys.argv[2], "rb"))
print svnParentmap.data
print sorted(svnParentmap.revsUsed())
converter = ParentMapConverter(svnParentmap)

rewritten_map = {}

allrefs = [repo.get_peeled(name) for name in repo.refs.allkeys()]
walker = repo.get_walker(include=allrefs)

commits = []
for (i,entry) in tqdm(enumerate(walker, 1), desc="Getting commits", unit="rev", leave=True):
    commits.append(entry.commit)
    converter.feedCommit(entry.commit)

parentmap = converter.doConversion()

newcommits = []

commits = reversed(commits)
for commit in tqdm(commits, desc="Processing commits", unit="rev", leave=True):
    oldid = commit.id
    parents = list(commit.parents)

    if oldid in parentmap:
        parents.extend(parentmap[oldid])

    for i in xrange(len(parents)):
        if parents[i] in rewritten_map:
            parents[i] = rewritten_map[parents[i]]

    if commit.parents != parents:
        commit.parents = parents
        rewritten_map[oldid] = commit.id
        newcommits.append((commit,None))

print "Saving new commits to repo..."
repo.object_store.add_objects(newcommits)
print "done"

refnames = repo.refs.allkeys()
for ref in sorted(refnames):
    sha = repo.refs[ref]
    obj = repo.get_object(sha)
    if isinstance(obj, dulwich.objects.Commit):
        if sha in rewritten_map:
            assert repo.refs.set_if_equals(ref, sha, rewritten_map[sha])
            print "Updated ref '%s'" % ref
        else:
            print "Unchanged ref '%s'" % ref
    elif isinstance(obj, dulwich.objects.Tag):
        tag = obj
        if tag.object[0] != dulwich.objects.Commit:
            print "WARNING: annotated tag '%s' points to a %s, not supported" % (ref, tag.object[0].type_name)
            continue
        if tag.object[1] in rewritten_map:
            assert tag.object[1] != rewritten_map[tag.object[1]]
            tag.object = (dulwich.objects.Commit, rewritten_map[tag.object[1]])
            assert tag.id != sha
            repo.object_store.add_object(tag)
            print "Rewritten tag '%s'" % ref
            repo.refs.set_if_equals(ref, sha, tag.id)
            print "Updated ref '%s'" % ref
        else:
            print "Unchanged tag '%s'" % ref
    else:
        print "WARNING: ref '%s' points to a %s, not supported yet" % (ref, obj.type_name)
